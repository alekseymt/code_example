import React from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';
import { connect } from 'react-redux';

class InfiniteScroll extends React.Component {
  componentDidMount() {
    ReactDOM.findDOMNode(this).addEventListener('scroll', this.onScroll);
  }

  componentWillUnmount() {
    ReactDOM.findDOMNode(this).removeEventListener('scroll', this.onScroll);
  }

  onScroll() {
    const { dispatch, loadMore } = this.props;

    if (this.shouldUpdate()) {
      dispatch(loadMore());
    }
  }

  shouldUpdate() {
    const { triggerTime, hasMore, isLoading } = this.props;
    const el = ReactDOM.findDOMNode(this);
    const now = new Date().getTime();

    const offset = el.scrollHeight - el.scrollTop - el.clientHeight;
    const delay = now - triggerTime;

    return offset < el.clientHeight && delay > 1000 && hasMore && !isLoading;
  }

  render() {
    const { children, className, tag } = this.props;
    const tagName = tag || 'div';
    const className = classnames('infinite-scroll', className);

    return React.createElement(tagName, { className }, children);
  }
}

InfiniteScroll.propTypes = {
  loadMore: React.PropTypes.func,
  isLoading: React.PropTypes.bool,
  hasMore: React.PropTypes.bool,
  triggerTime: React.PropTypes.number,
};

export default connect()(InfiniteScroll);
